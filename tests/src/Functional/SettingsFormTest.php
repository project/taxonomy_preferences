<?php

namespace Drupal\Tests\taxonomy_preferences\Functional;

use Drupal\taxonomy\Entity\Vocabulary;
use Drupal\Tests\BrowserTestBase;

/**
 * Tests Taxonomy Preferences Settings.
 *
 * @group taxonomy_preferences
 */
class SettingsFormTest extends BrowserTestBase {

  /**
   * Disabled config schema checking temporarily until all errors are resolved.
   *
   * @var bool
   */
  protected $strictConfigSchema = FALSE;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'taxonomy_preferences',
    'node',
    'taxonomy',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'claro';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Create a tags vocabulary for the 'article' content type.
    $vocabulary = Vocabulary::create([
      'name' => 'Tags',
      'vid' => 'tags',
    ]);
    $vocabulary->save();

    $this->values = [
      'vocabulary' => ["tags" => 'tags'],
    ];

    $this->config('taxonomy_preferences.settings')->setData($this->values)->save();
  }

  /**
   * Test access to configuration page.
   */
  public function testCanAccessConfigPage() {
    $account = $this->drupalCreateUser([
      'access taxonomy preferences settings',
      'access content',
    ]);

    $this->drupalLogin($account);
    $this->drupalGet('/admin/config/system/taxonomy_preferences');
  }

}
