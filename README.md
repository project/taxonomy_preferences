## CONTENTS OF THIS FILE

 - Introduction
 - Requirements
 - Installation
 - Configuration
 - Maintainers


## INTRODUCTION

- The Taxonomy Preferences (taxonomy_preferences) module provides a block for end users
to choose their preferences. The module has a configuration page where site administrators
can choose which taxonomy terms can be selected. The end user marks his selection and submit the values.
The values are then stored in session using the php super global $_SESSION for future use.

This module also provides two user permissions, one for accessing the settings page and other to be able to see the created block

## REQUIREMENTS

- This module requires no modules outside of Drupal core. However, it was developed to use combined with
Views Extras module since Views Extra has an option to provide a contextual filter to a view based on session variables.

## INSTALLATION

- Install as you would normally install a contributed Drupal module. Visit
https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
for further information.


## CONFIGURATION

 - First navigate to

   Configuration » System » Taxonomy Preferences

   - Select the vocabularies;

   - Select the terms to provide to the block;

   - You can also configure a string to be displayed. This string is translatable using the translate tab;

   - Click Save Configuration.

- A block is available to display using Block Layout

   - Configure where you want the block to be displayed.

- After the user submit his preferences the term ids are stored in $_SESSION['taxonomy_preferences']['preferences_key']

   - When using Views Extras to filter views based on taxonomy terms use "taxonomy_preferences::preferences_key" in the field "Session variable key" provided by Views Extras.

## MAINTAINERS

Current maintainers:
 * Nelson Alves (nsalves) - https://www.drupal.org/u/nsalves

This project has been sponsored by:
 * NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate.
