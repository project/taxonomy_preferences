<?php

namespace Drupal\taxonomy_preferences\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Path\PathMatcherInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implements user preferences form to be added to a block.
 */
class TaxonomyPreferencesForm extends FormBase {

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The path matcher service.
   *
   * @var \Drupal\Core\Path\PathMatcherInterface
   */
  protected $pathMatcher;

  /**
   * TaxonomyPreferencesForm constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Path\PathMatcherInterface $path_matcher
   *   The path matcher service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, EntityRepositoryInterface $entity_repository, ConfigFactoryInterface $config_factory, PathMatcherInterface $path_matcher) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->entityRepository = $entity_repository;
    $this->configFactory = $config_factory;
    $this->pathMatcher = $path_matcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository'),
      $container->get('config.factory'),
      $container->get('path.matcher')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_preferences_form_block';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // List of terms id from the settings form.
    $selected_taxonomies_id = $this->configFactory->get('taxonomy_preferences.settings')->get('terms');
    // Get current language.
    $language = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    // Get configuration translation in the current language.
    $config_translation = $this->languageManager->getLanguageConfigOverride($language, 'taxonomy_preferences.settings');
    // This is needed since getLanguageConfigOverride returns NULL when using
    // the default language.
    if ($user_message = $config_translation->get('user_message')) {
      $user_message = $config_translation->get('user_message');
    }
    else {
      $user_message = $this->configFactory->get('taxonomy_preferences.settings')->get('user_message');
    }

    foreach ($selected_taxonomies_id as $id) {
      // Load taxonomy by id.
      $term = $this->entityTypeManager->getStorage('taxonomy_term')->load($id);
      // Get taxonomy translated term.
      $translated_term = $this->entityRepository->getTranslationFromContext($term, $language)->getName();
      $term_list[$term->id()] = $translated_term;
    }

    $form['preferences_header'] = [
      '#prefix' => '<p class="taxonomy-preferences-message">',
      '#suffix' => '</p>',
      '#markup' => $user_message,
    ];

    $form['taxonomy_terms'] = [
      '#type' => 'checkboxes',
      '#required' => TRUE,
      '#options' => $term_list,
      '#prefix' => '<div class="taxonomy-preferences-container">',
      '#suffix' => '</div>',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
      '#prefix' => '<div class="taxonomy-preferences-save-button">',
      '#suffix' => '</div>',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Emits and error if no values are selected on submitting the form.
    if (empty(array_filter($form_state->getValue('taxonomy_terms')))) {
      $form_state->setErrorByName('preferences_header', $this->t('Please select your preferences and submit the form again.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    // Get all taxonomy ids that were selected on the form.
    $preferences = array_filter($form_state->getValue('taxonomy_terms'));
    // Concatenate each taxonomy id with a '+'.
    $preferences = implode('+', $preferences);
    // Set the taxonomy ids in session.
    $_SESSION['taxonomy_preferences']['preferences_key'] = $preferences;
    // Set a variable to true when the form is submitted.
    $_SESSION['taxonomy_preferences']['visibility'] = TRUE;
    // To avoid redirecting to /node when in frontpage.
    if ($this->pathMatcher->isFrontPage()) {
      $form_state->setRedirect('<front>');
    }
  }

}
