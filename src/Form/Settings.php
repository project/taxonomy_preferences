<?php

namespace Drupal\taxonomy_preferences\Form;

use Drupal\Core\Entity\EntityRepositoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure the Taxonomy Preferences settings.
 */
class Settings extends ConfigFormBase {

  /**
   * Provides an interface for entity type managers.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The language manager service.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  protected $languageManager;

  /**
   * The entity repository service.
   *
   * @var \Drupal\Core\Entity\EntityRepositoryInterface
   */
  protected $entityRepository;

  /**
   * ReportWorkerBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   Provides an interface for entity type managers.
   * @param \Drupal\Core\Language\LanguageManagerInterface $language_manager
   *   The language manager service.
   * @param \Drupal\Core\Entity\EntityRepositoryInterface $entity_repository
   *   The entity repository service.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, LanguageManagerInterface $language_manager, EntityRepositoryInterface $entity_repository) {
    $this->entityTypeManager = $entity_type_manager;
    $this->languageManager = $language_manager;
    $this->entityRepository = $entity_repository;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('language_manager'),
      $container->get('entity.repository')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'taxonomy_preferences_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['taxonomy_preferences.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $no_js_use = FALSE) {
    // Get current configuration.
    $config = $this->config('taxonomy_preferences.settings');
    // Get current language.
    $language = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    // Load vocabulary list.
    $vocabulary = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();

    foreach ($vocabulary as $vocabulary_id => $vocabulary_obj) {
      $vocabulary_list[$vocabulary_id] = $vocabulary_obj->label();
    }

    $form['vocabulary'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Vocabularies list'),
      '#description' => $this->t('Select the vocabularies from which the terms can be selected.'),
      '#required' => TRUE,
      '#options' => $vocabulary_list,
      '#default_value' => $config->get('vocabulary'),
      '#ajax' => [
        'callback' => '::termCallback',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'edit-output',
        'method' => 'replace',
        'progress' => [
          'type' => 'throbber',
          'message' => $this->t('Verifying...'),
        ],
      ],
    ];

    $selected_vocabulary = array_filter($config->get('vocabulary'));
    $term_list = [];
    foreach ($selected_vocabulary as $key) {
      // List of terms for each vocabulary.
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($key);
      foreach ($terms as $term) {
        // Load each term.
        $taxonomy_term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term->tid);
        // Get translation for each term.
        $translated_term = $this->entityRepository->getTranslationFromContext($taxonomy_term, $language)->getName();
        $term_list[$term->tid] = $translated_term;
      }
    }

    $form['terms'] = [
      '#type' => 'select',
      '#multiple' => TRUE,
      '#title' => $this->t('Taxonomy terms'),
      '#description' => $this->t('Select the terms that you wish to use as preferences and display to the user.'),
      '#required' => TRUE,
      '#options' => $term_list,
      '#default_value' => $config->get('terms'),
      '#validated' => TRUE,
      '#prefix' => '<div id="edit-output">',
      '#suffix' => '</div>',
    ];

    $form['user_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('User Message'),
      '#description' => $this->t('The message to be displayed to the user when selecting preferences. This is the default value, use translation tab to translate this field'),
      '#default_value' => $config->get('user_message') ?: '',
    ];

    $form_state->setRebuild(TRUE);
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->configFactory->getEditable('taxonomy_preferences.settings')
      ->set('vocabulary', $form_state->getValue('vocabulary'))
      ->set('terms', $form_state->getValue('terms'))
      ->set('user_message', $form_state->getValue('user_message'))
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Add Term list ajax callback.
   */
  public function termCallback(array &$form, FormStateInterface $form_state) {

    $selectedValue = array_filter($form_state->getValue('vocabulary'));
    $term_list = [];
    // Get current language.
    $language = $this->languageManager->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
    foreach ($selectedValue as $key => $value) {
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadTree($key);
      foreach ($terms as $term) {
        $taxonomy_term = $this->entityTypeManager->getStorage('taxonomy_term')->load($term->tid);
        $translated_term = $this->entityRepository->getTranslationFromContext($taxonomy_term, $language)->getName();
        $term_list[$term->tid] = $translated_term;
      }
    }

    $form['terms']['#options'] = $term_list;
    return $form['terms'];
  }

}
